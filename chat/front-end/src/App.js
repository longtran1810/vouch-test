import React from 'react';
import {BrowserRouter as Router, Switch, Route} from 'react-router-dom';
import io from 'socket.io-client';
// Comp
import Chat from './pages/chat/chat';
import Home from './pages/home/home';
// Styling
import './App.scss';

const socket = io.connect ('/');
function ChatApp ({match: {params}}) {
  return (
    <Chat
      username={params.username}
      roomname={params.roomname}
      socket={socket}
    />
  );
}
function App () {
  return (
    <Router>
      <div className="App">
        <Switch>
          <Route path="/" exact>
            <Home socket={socket} />
          </Route>
          <Route path="/chat/:roomname/:username" component={ChatApp} />
        </Switch>
      </div>
    </Router>
  );
}

export default App;
