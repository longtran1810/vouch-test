import React, {useState} from 'react';
import {useHistory} from 'react-router-dom';
import './home.scss';

function Homepage({socket}) {
  const [username, setusername] = useState ('');
  const [roomname, setroomname] = useState ('');
  const history = useHistory ();
  // 
  const sendData = () => {
    if (username !== '' && roomname !== '') {
      fetch (`/api/user/check-user-name/${username}`)
        .then (res => res.json ())
        .then (res => {
          if (res.statusCode === 200) {
            alert ('The username has available on the system');
            setusername ('');
            return;
          }
          socket.emit ('joinRoom', {username, roomname});
          history.push (`/chat/${roomname}/${username}`);
        })
        .catch (() => {
          return;
        });
    }
  };

  return (
    <div className="chat-home">
      <h1>Join Chatroom</h1>
      <input
        placeholder="Username"
        value={username}
        onChange={e => setusername (e.target.value)}
      />
      <input
        placeholder="RoomId"
        value={roomname}
        onChange={e => setroomname (e.target.value)}
      />
      <button onClick={sendData}>JOIN</button>
    </div>
  );
}

export default Homepage;
