import React, {useState, useEffect, useRef} from 'react';
import {decrypt_text, encrypt_text} from '../../helpers/aes.js';
import { IconSend } from '../../components/Icon';
import './chat.scss';

function Chat({username, roomname, socket}) {
  const [text, setText] = useState ('');
  const [messages, setMessages] = useState ([]);
  const messagesEndRef = useRef (null);

  useEffect (
    () => {
      socket.on ('message', data => {
        setMessages (prev => [...prev, {
          userId: data.userId,
          username: data.username,
          text: decrypt_text (data.text, data.username),
        }]);
      });
    },
    [socket]
  );

  const sendData = () => {
    if (text !== '') {
      const ans = encrypt_text (text);
      socket.emit ('chat', ans);
      setText ('');
    }
  };

  useEffect (
    () => {
      messagesEndRef.current.scrollIntoView ({behavior: 'smooth'});
    },
    [messages, messagesEndRef]
  );

  return (
    <div className="chat_wrap">
      <div className="chat_wrap_title">
        <h2>
          {username} <span style={{fontSize: '30px', color: "#000"}}>in Room {roomname}</span>
        </h2>
      </div>
      <div className="chat-msg">
        {messages?.map (
          (m, index) =>
            m.username === username
              ? <div className="chat-msg_msg chat-msg_msg--right" key={m.userId + index + m.text}>
                  <p>{m.text} </p>
                </div>
              : <div className="chat-msg_msg" key={m.userId + index + m.text}>
                  <span>{m.username}</span>
                  <p>{m.text}</p>
                </div>
        )}
        <div ref={messagesEndRef} />
      </div>
      <div className="sent_message_wrap">
        <input
          placeholder="Message here..."
          value={text}
          onChange={e => setText (e.target.value)}
          onKeyPress={e => {
            if (e.key === 'Enter') {
              sendData ();
            }
          }}
        />
        <IconSend className="sent_message_icon" onClick={(sendData)} />
      </div>
    </div>
  );
}
export default Chat;
