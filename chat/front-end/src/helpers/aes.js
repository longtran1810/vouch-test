const aes256 = require("aes256");

const secret_key = "testHelloworld";

export const encrypt_text = (text = "") => {
  return  aes256.encrypt(secret_key, text);;
};
export const decrypt_text = (aseText ="", username) => {
  if (aseText.startsWith("Welcome") || aseText.startsWith(username)) {
    return aseText;
  }

  return aes256.decrypt(secret_key, aseText);;
};
