const { User } = require('./models/User')

async function join_user (id, username, room) {
  const newUser = await new User({
    socket: id,
    username,
    room
  }).save();

  return newUser;
}

async function get_user (socket_id) {
  return await User.findOne({socket: socket_id}).exec()
}

async function user_disconnect (socket_id) {
  return await User.findOneAndDelete({socket: socket_id})
}

module.exports = {
  join_user,
  get_user,
  user_disconnect,
};
