const mongoose = require('mongoose');

mongoose.Promise = global.Promise;

const connect = () => {
    mongoose.connect(
        process.env.DB_URL || 'mongodb://Localhost:27017',
        err => {
            if (err) {
                console.log(`[ERR] ${err}`)
                return err;
            }
        }
    );
};

connect();

module.exports = { mongoose, connect };
