const mongoose = require ('mongoose');
const UserSchema = new mongoose.Schema (
  {
    username: {
      type: String,
      trim: true,
      unique: true,
      required: true,
      maxlength: ['15', 'Username should be less than 15 characters'],
    },
    room: {
      type: Number,
      trim: true,
      required: true
    },
    socket: {
      type: String,
      required: true
    }
  },
  {
    timestamps: {
      createdAt: 'created_at',
      updatedAt: 'updated_at',
    },
  }
);

const User = mongoose.model('User', UserSchema);

module.exports = { User };
