const express = require("express");
const app = express();
const socket = require("socket.io");
const cors = require("cors");

/** MongoDB **/
require('./db/mongoose');

const { get_user, user_disconnect, join_user } = require("./user");

app.use(express());

const port = 8000;

app.use(cors());

/** Routes */
const endPoints = require('./routes/apis');
app.use('/api', endPoints);

var server = app.listen(
  port,
  console.log(
    `Server is running on the port no: ${(port)} `
  )
);

const io = socket(server);

// Region: Socket
io.on("connection", (socket) => {
  //Socket: Join room
  socket.on("joinRoom", async ({ username, roomname }) => {
    // create user
    const new_user = await join_user(socket.id, username, roomname);
    console.log(new_user.username, "new");
    socket.join(new_user.room);

    // Display welcome
    socket.emit("message", {
      userId: new_user.id,
      username: new_user.username,
      text: `Welcome ${new_user.username}`,
    });

    // Inform joined room
    socket.broadcast.to(new_user.room).emit("message", {
      userId: new_user.id,
      username: new_user.username,
      text: `${new_user.username} has joined the chat`,
    });
  });

  // Socket: Chat
  socket.on("chat", async (text) => {
    const user = await get_user(socket.id);
    io.to(user.room).emit("message", {
      userId: user.id,
      username: user.username,
      text: text,
    });
  });

  // Socket: Exit room
  socket.on("disconnect", async () => {
    // Disconnected display
    const user = await user_disconnect(socket.id);

    if (user) {
      io.to(user.room).emit("message", {
        userId: user.id,
        username: user.username,
        text: `${user.username} has left the chat`,
      });
    }
  });
});