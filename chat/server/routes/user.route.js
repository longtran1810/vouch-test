const express = require ('express');
const {User} = require ('../models/User');
const router = express.Router ();

router.get ('/check-user-name/:username', async (req, res) => {
  const user = await User.findOne ({username: req.params.username}).exec ();  
  console.log(user)
  if (user) {
    return res.json({message: "User has available", statusCode: 200})
  }
  return res.json({message: "Ok. username can use", statusCode: 404})
});

module.exports = router;
