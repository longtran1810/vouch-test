## FE Source code
```sh
cd frontend
yarn install && yarn run start
FE will start on port 3456
```
## Start server
```sh
cd server
yarn install && yarn start
```

> Because I don't have much time to do it, I make it as simple as possible, thanks for the review